<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <style type ="text/css" >
        .footer{ 
            position: fixed;     
            text-align: center;
            bottom: 0px;
            width: 100%;
            font-size: 16px;
            background: rgb(33, 37, 41);
            color: rgb(180, 180, 180);
        }  
     </style>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('navi.bar')
        <div class="container">
            @include('navi.alert')
            @yield('content')
        </div>
        <div class="footer">185150700111020 - Ryan Gatutkaca Qalbi Ramadhan</div>
    </div>
</body>
</html>
