@extends('layouts.app')
@section('content')
    <h1>Data Mahasiswa</h3>
    @if(count($data) > 0)
        @foreach($data as $siswa)
            <div class="well">
                <h3><a href="/data/{{$siswa->id}}">{{$siswa->nama}}</a></h3>
                <small>Ditambahkan pada {{$siswa->created_at}}</small>
            </div>
        @endforeach
    @else
        <p>Tidak ditemukan data</p>
    @endif
@endsection