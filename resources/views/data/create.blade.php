@extends('layouts.app')
@section('content')
    <h1>Tambah Data</h1>
    {!! Form::open(['action' => 'App\Http\Controllers\PostsController@store', 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('nim', 'NIM')}}
        {{Form::text('nim', '', ['class' => 'form-control', 'placeholder' => 'NIM'])}}
    </div>
    <div class="form-group">
        {{Form::label('nama', 'Nama')}}
        {{Form::text('nama', '', ['class' => 'form-control', 'placeholder' => 'Nama'])}}
    </div>
    <div class="form-group">
        {{Form::label('alamat', 'Alamat')}}
        {{Form::text('alamat', '', ['class' => 'form-control', 'placeholder' => 'Alamat'])}}
    </div>
    <br>
    {{Form::submit('Simpan', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection