@extends('layouts.app')
@section('content')
    <br>
    <a href="/data" class="btn btn-secondary">Kembali</a>
    <h1>{{$siswa->title}}</h1>
    <div>
        NIM&emsp;&ensp;: {{$siswa->nim}}<br>
        Nama&ensp;&nbsp;: {{$siswa->nama}}<br>
        Alamat : {{$siswa->alamat}}    
    </div>
    <br>
    <small>Ditambahkan pada {{$siswa->created_at}}</small>
    <hr>
    @if(!Auth::guest())
    <a href="/data/{{$siswa->id}}/edit" class="btn btn-success">Edit</a>
    {!!Form::open(['action' => ['App\Http\Controllers\PostsController@destroy', $siswa->id], 'method' => 'POST', 'class' => 'float-end'])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}
    @endif
@endsection