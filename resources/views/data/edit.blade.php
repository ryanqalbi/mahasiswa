@extends('layouts.app')
@section('content')
    <h1>Edit Data</h1>
    {!! Form::open(['action' => ['App\Http\Controllers\PostsController@update', $siswa->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('nim', 'NIM')}}
        {{Form::text('nim', $siswa->nim, ['class' => 'form-control', 'placeholder' => 'NIM'])}}
    </div>
    <div class="form-group">
        {{Form::label('nama', 'Nama')}}
        {{Form::text('nama', $siswa->nama, ['class' => 'form-control', 'placeholder' => 'Nama'])}}
    </div>
    <div class="form-group">
        {{Form::label('alamat', 'Alamat')}}
        {{Form::text('alamat', $siswa->alamat, ['class' => 'form-control', 'placeholder' => 'Alamat'])}}
    </div>
    <br>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Simpan', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection