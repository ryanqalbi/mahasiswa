<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'data';
    public $primaryKey = 'id';
    public $timestamps = true;
}
